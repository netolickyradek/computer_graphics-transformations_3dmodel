package render;

import model.Line;
import rasterize.LineRasterizer;
import rasterize.RasterBufferImage;
import solids.Axis;
import solids.Solid;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec3D;

import java.util.List;

public class WireRenderer {

    private final LineRasterizer lineRasterizer;
    private final RasterBufferImage raster;
    private Mat4 proj;
    private Mat4 camera;

    public WireRenderer(RasterBufferImage raster, LineRasterizer lineRasterizer, Mat4 proj, Mat4 camera) {
        this.lineRasterizer = lineRasterizer;
        this.raster = raster;
        this.proj = proj;
        this.camera = camera;
    }

    /**
     * Method displays specific solid with its color on screen from the camera position and projection
     *
     * @param solid - rendered solid
     */
    public void renderSolid(Solid solid) {
        // MVP matrix
        Mat4 mvp = solid.getModel().mul(camera).mul(proj);

        for (int i = 0; i < solid.getIb().size(); i = i + 2) {
            int index1 = solid.getIb().get(i);
            int index2 = solid.getIb().get(i + 1);

            Point3D point1 = solid.getVb().get(index1);
            Point3D point2 = solid.getVb().get(index2);

            Point3D point1Trans = point1.mul(mvp);
            Point3D point2Trans = point2.mul(mvp);

            // fast clip by w coordinates
            double w = point1Trans.getW();
            double x = point1Trans.getX();
            double y = point1Trans.getY();
            double z = point1Trans.getZ();

            if (!(-w <= x && x <= w && -w <= y && y <= w && 0 <= z && z <= w)) {
                continue;
            }

            w = point2Trans.getW();
            x = point2Trans.getX();
            y = point2Trans.getY();
            z = point2Trans.getZ();

            if (!(-w <= x && x <= w && -w <= y && y <= w && 0 <= z && z <= w)) {
                continue;
            }

            // Dehomogenization
            Point3D point1Dehomog = point1Trans.mul(1 / point1Trans.getW());
            Point3D point2Dehomog = point2Trans.mul(1 / point2Trans.getW());

            // Transformation to screen
            Vec3D v1 = transToWindow(new Vec3D(point1Dehomog));
            Vec3D v2 = transToWindow(new Vec3D(point2Dehomog));

            // Rasterization of each line
            Line line = new Line(
                    (int) Math.round(v1.getX()), (int) Math.round(v1.getY()),
                    (int) Math.round(v2.getX()), (int) Math.round(v2.getY()), solid.getColor());
            lineRasterizer.rasterize(line);
        }
    }

    /**
     * Transforms dehomogenized points to screen
     *
     * @param vec3D - vector from point
     * @return transformed vector
     */
    private Vec3D transToWindow(Vec3D vec3D) {
        int width = raster.getImg().getWidth() / 2;
        int height = raster.getImg().getHeight() / 2;
        return vec3D.mul(new Vec3D(1, -1, 1)).add(new Vec3D(1, 1, 0)).mul(new Vec3D(width, height, 0));
    }

    /**
     * Goes through solids and renders whole scene at once
     *
     * @param solids - list of solids to render
     */
    public void renderScene(List<Solid> solids) {
        for (Solid solid : solids) {
            renderSolid(solid);
        }
    }

    /**
     * Renders all three axis with their specific color
     *
     * @param x axis
     * @param y axis
     * @param z axis
     */
    public void renderAxis(Axis x, Axis y, Axis z) {
        renderSolid(x);
        renderSolid(y);
        renderSolid(z);
    }


    // Getters and setters
    public void setCamera(Mat4 camera) {
        this.camera = camera;
    }

    public void setProj(Mat4 proj) {
        this.proj = proj;
    }

    public Mat4 getProj() {
        return proj;
    }
}
