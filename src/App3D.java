import rasterize.LineRasterizer;
import rasterize.LineRasterizerGraphics;
import rasterize.RasterBufferImage;
import render.WireRenderer;
import solids.*;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class App3D {
    private final JPanel panel;
    private final RasterBufferImage raster;
    private final WireRenderer wireRenderer;
    // Scene
    private List<Solid> scene;
    private Solid cube;
    private Solid pyramid;
    private Solid prism;
    private Camera camera;

    // Axis
    private final Axis x;
    private final Axis y;
    private final Axis z;

    private int oldX, oldY;
    private int editedSolid;
    private boolean isRotating = false;


    public App3D(int width, int height) {
        JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("PGRF1 - TASK 3 NETOLICKÝ RADEK");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferImage(800, 600);
        LineRasterizer lineRasterizer = new LineRasterizerGraphics(raster);

        // scene
        cube = new Cube(0xff0000);
        pyramid = new Pyramid(0x00ff00);
        prism = new Prism(0x00ff00);
        x = new Axis("x");
        y = new Axis("y");
        z = new Axis("z");
        scene = new ArrayList<>();
        scene.add(cube);
        scene.add(pyramid);
        scene.add(prism);

        editedSolid = 1;

        camera = new Camera(new Vec3D(-6, 0.5, 1.5),
                //azimuth
                Math.toRadians(0),
                //zenith
                Math.toRadians(0),
                10,
                true
        );

        // Perspective and orthogonal projection
        Mat4 projPersp = new Mat4PerspRH(Math.toRadians(60), height / (float) width, 0.1, 200f);
        Mat4 projOrtho = new Mat4OrthoRH(width / 50., height / 50., 0.1, 200f);

        wireRenderer = new WireRenderer(raster, lineRasterizer, projPersp, camera.getViewMatrix());
        panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                raster.present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.requestFocus();
        panel.requestFocusInWindow();
        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int key = e.getKeyCode();

                // animation
                if (key == KeyEvent.VK_SPACE) {
                    isRotating = !isRotating;
                }

                if (key == KeyEvent.VK_W) {
                    camera = camera.forward(0.2);
                }
                if (key == KeyEvent.VK_A) {
                    camera = camera.left(0.2);
                }
                if (key == KeyEvent.VK_S) {
                    camera = camera.backward(0.2);
                }
                if (key == KeyEvent.VK_D) {
                    camera = camera.right(0.2);
                }
                if (key == KeyEvent.VK_SHIFT) {
                    camera = camera.up(0.2);
                }
                if (key == KeyEvent.VK_CONTROL) {
                    camera = camera.down(0.2);
                }

                // switching solids to edit
                if (key == KeyEvent.VK_NUMPAD1) {
                    //cube
                    editedSolid = 1;
                    cube.setColor(0xff0000);
                    prism.setColor(0x00ff00);
                    pyramid.setColor(0x00ff00);
                }
                if (key == KeyEvent.VK_NUMPAD2) {
                    //pyramid
                    editedSolid = 2;
                    cube.setColor(0x00ff00);
                    pyramid.setColor(0xff0000);
                    prism.setColor(0x00ff00);
                }
                if (key == KeyEvent.VK_NUMPAD3) {
                    //prism
                    editedSolid = 3;
                    cube.setColor(0x00ff00);
                    pyramid.setColor(0x00ff00);
                    prism.setColor(0xff0000);
                }

                // rotating/translating selected solid
                if (editedSolid == 1) {
                    if (key == KeyEvent.VK_X) {
                        cube.setModel(cube.rotateX());
                    }
                    if (key == KeyEvent.VK_Y) {
                        cube.setModel(cube.rotateY());
                    }
                    if (key == KeyEvent.VK_Z) {
                        cube.setModel(cube.rotateZ());
                    }
                    if (key == KeyEvent.VK_UP) {
                        cube.setModel(cube.moveXUp());
                    }
                    if (key == KeyEvent.VK_DOWN) {
                        cube.setModel(cube.moveXDown());
                    }
                    if (key == KeyEvent.VK_LEFT) {
                        cube.setModel(cube.moveYUp());
                    }
                    if (key == KeyEvent.VK_RIGHT) {
                        cube.setModel(cube.moveYDown());
                    }
                    if (key == KeyEvent.VK_ENTER) {
                        cube.setModel(cube.moveZUp());
                    }
                    if (key == KeyEvent.VK_BACK_SPACE) {
                        cube.setModel(cube.moveZDown());
                    }
                }

                if (editedSolid == 2) {
                    if (key == KeyEvent.VK_X) {
                        pyramid.setModel(pyramid.rotateX());
                    }
                    if (key == KeyEvent.VK_Y) {
                        pyramid.setModel(pyramid.rotateY());
                    }
                    if (key == KeyEvent.VK_Z) {
                        pyramid.setModel(pyramid.rotateZ());
                    }
                    if (key == KeyEvent.VK_UP) {
                        pyramid.setModel(pyramid.moveXUp());
                    }
                    if (key == KeyEvent.VK_DOWN) {
                        pyramid.setModel(pyramid.moveXDown());
                    }
                    if (key == KeyEvent.VK_LEFT) {
                        pyramid.setModel(pyramid.moveYUp());
                    }
                    if (key == KeyEvent.VK_RIGHT) {
                        pyramid.setModel(pyramid.moveYDown());
                    }
                    if (key == KeyEvent.VK_ENTER) {
                        pyramid.setModel(pyramid.moveZUp());
                    }
                    if (key == KeyEvent.VK_BACK_SPACE) {
                        pyramid.setModel(pyramid.moveZDown());
                    }
                }

                if (editedSolid == 3) {
                    if (key == KeyEvent.VK_X) {
                        prism.setModel(prism.rotateX());
                    }
                    if (key == KeyEvent.VK_Y) {
                        prism.setModel(prism.rotateY());
                    }
                    if (key == KeyEvent.VK_Z) {
                        prism.setModel(prism.rotateZ());
                    }
                    if (key == KeyEvent.VK_UP) {
                        prism.setModel(prism.moveXUp());
                    }
                    if (key == KeyEvent.VK_DOWN) {
                        prism.setModel(prism.moveXDown());
                    }
                    if (key == KeyEvent.VK_LEFT) {
                        prism.setModel(prism.moveYUp());
                    }
                    if (key == KeyEvent.VK_RIGHT) {
                        prism.setModel(prism.moveYDown());
                    }
                    if (key == KeyEvent.VK_ENTER) {
                        prism.setModel(prism.moveZUp());
                    }
                    if (key == KeyEvent.VK_BACK_SPACE) {
                        prism.setModel(prism.moveZDown());
                    }
                }


                // reset button
                if (key == KeyEvent.VK_R) {
                    camera = new Camera(new Vec3D(-6, 0.5, 1.5), Math.toRadians(0), Math.toRadians(0), 1, true);
                    cube = new Cube(0x00ffff);
                    pyramid = new Pyramid(0xffff00);
                    prism = new Prism(0xffff00);
                    editedSolid = 1;
                    scene = new ArrayList<>();
                    scene.add(cube);
                    scene.add(pyramid);
                    scene.add(prism);
                }

                // Switch projection
                if (key == KeyEvent.VK_P) {
                    if (wireRenderer.getProj() == projPersp) {
                        wireRenderer.setProj(projOrtho);
                    } else {
                        wireRenderer.setProj(projPersp);
                    }
                }

                update();
            }
        });

        panel.addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                super.mouseWheelMoved(e);
                int notches = e.getWheelRotation();
                if (notches < 0) {
                    for (Solid s : scene) {
                        s.setModel(s.scaleUp());
                    }
                } else {
                    for (Solid s : scene) {
                        s.setModel(s.scaleDown());
                    }
                }
                update();
            }
        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                oldX = e.getX();
                oldY = e.getY();
            }

        });

        panel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int dX = oldX - e.getX();
                int dY = oldY - e.getY();

                double azimuth = Math.PI * (dX / (double) width);
                double zenith = Math.PI * (dY / (double) height);

                camera = camera.addAzimuth(azimuth);
                camera = camera.addZenith(zenith);

                if (camera.getZenith() > 90)
                    camera = camera.withZenith(90);
                if (camera.getZenith() < -90)
                    camera = camera.withZenith(-90);

                oldX = e.getX();
                oldY = e.getY();

                update();
            }
        });
    }

    public void start() {
        // Start the animation loop
        Timer timer = new Timer(30, e -> {
            if (isRotating) {
                rotateSelectedSolid();
                update();
            }
        });
        timer.start();
        update();
    }

    public void update() {
        raster.clear();

        wireRenderer.setCamera(camera.getViewMatrix());
        wireRenderer.renderAxis(x, y, z);
        wireRenderer.renderScene(scene);

        panel.repaint();
    }

    private void rotateSelectedSolid() {
        switch (editedSolid) {
            case 1:
                cube.setModel(cube.rotateX());
                cube.setModel(cube.rotateY());
                cube.setModel(cube.rotateZ());
                break;
            case 2:
                pyramid.setModel(pyramid.rotateX());
                pyramid.setModel(pyramid.rotateY());
                pyramid.setModel(pyramid.rotateZ());
                break;
            case 3:
                prism.setModel(prism.rotateX());
                prism.setModel(prism.rotateY());
                prism.setModel(prism.rotateZ());
                break;
        }
    }
}
