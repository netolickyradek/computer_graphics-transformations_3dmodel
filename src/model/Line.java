package model;

/**
 * Class used to define lines by two Points and color.
 */
public class Line {
    private final Point point1;
    private final Point point2;
    private final int lineColor;


    public Line(int x1, int y1, int x2, int y2, int lineColor) {
        this.point1 = new Point(x1, y1);
        this.point2 = new Point(x2, y2);
        this.lineColor = lineColor;
    }

    public Point getPoint1() {
        return point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public int getLineColor() {
        return lineColor;
    }
}

