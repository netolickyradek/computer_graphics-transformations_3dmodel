package solids;

import transforms.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Solid {
    private int color;

    protected final List<Point3D> vb = new ArrayList<>();
    protected final List<Integer> ib = new ArrayList<>();

    private Mat4 model = new Mat4Identity();

    /**
     * Adds any number of indices to index buffer
     *
     * @param indices - integer values of indices
     */
    protected void addIndices(Integer... indices) {
        ib.addAll(Arrays.asList(indices));
    }

    // Solid rotation methods for each axis
    public Mat4 rotateX() {
        Mat4RotX mat = new Mat4RotX(Math.toRadians(5));
        return getModel().mul(mat);
    }

    public Mat4 rotateY() {
        Mat4RotY mat = new Mat4RotY(Math.toRadians(5));
        return getModel().mul(mat);
    }

    public Mat4 rotateZ() {
        Mat4RotZ mat = new Mat4RotZ(Math.toRadians(5));
        return getModel().mul(mat);
    }

    // Solid scaling methods
    public Mat4 scaleUp() {
        Mat4Scale mat = new Mat4Scale(1.1);
        return getModel().mul(mat);
    }

    public Mat4 scaleDown() {
        Mat4Scale mat = new Mat4Scale(0.9);
        return getModel().mul(mat);
    }

    // Solid translation methods for each axis
    public Mat4 moveXUp() {
        Mat4Transl mat = new Mat4Transl(new Vec3D(1, 0, 0));
        return getModel().mul(mat);
    }

    public Mat4 moveXDown() {
        Mat4Transl mat = new Mat4Transl(new Vec3D(-1, 0, 0));
        return getModel().mul(mat);
    }

    public Mat4 moveYUp() {
        Mat4Transl mat = new Mat4Transl(new Vec3D(0, 1, 0));
        return getModel().mul(mat);
    }

    public Mat4 moveYDown() {
        Mat4Transl mat = new Mat4Transl(new Vec3D(0, -1, 0));
        return getModel().mul(mat);
    }

    public Mat4 moveZUp() {
        Mat4Transl mat = new Mat4Transl(new Vec3D(0, 0, 1));
        return getModel().mul(mat);
    }

    public Mat4 moveZDown() {
        Mat4Transl mat = new Mat4Transl(new Vec3D(0, 0, -1));
        return getModel().mul(mat);
    }


    public List<Point3D> getVb() {
        return vb;
    }

    public List<Integer> getIb() {
        return ib;
    }

    public Mat4 getModel() {
        return model;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
