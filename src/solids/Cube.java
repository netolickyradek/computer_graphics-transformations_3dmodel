package solids;

import transforms.Point3D;

public class Cube extends Solid {
    private int color;

    public Cube(int color) {
        this.color = color;
        // Geometry
        vb.add(new Point3D(-1, -1, 1));
        vb.add(new Point3D(1, -1, 1));
        vb.add(new Point3D(1, 1, 1));
        vb.add(new Point3D(-1, 1, 1));
        vb.add(new Point3D(-1, -1, 3));
        vb.add(new Point3D(1, -1, 3));
        vb.add(new Point3D(1, 1, 3));
        vb.add(new Point3D(-1, 1, 3));

        // Topology
        addIndices(0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7);
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }
}
