package solids;

import transforms.Point3D;

public class Pyramid extends Solid {
    private int color;

    public Pyramid(int color) {
        this.color = color;

        // Geometry
        vb.add(new Point3D(-1, -3, 0));
        vb.add(new Point3D(1, -3, 0));
        vb.add(new Point3D(1, -1, 0));
        vb.add(new Point3D(-1, -1, 0));
        vb.add(new Point3D(0, -2, 2));

        // Topology
        addIndices(0, 1, 1, 2, 2, 3, 3, 0, 0, 4, 1, 4, 2, 4, 3, 4);
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }
}
