package solids;

import transforms.Point3D;

public class Prism extends Solid {
    private int color;

    public Prism(int color) {
        this.color = color;

        // Geometry
        vb.add(new Point3D(-1, 2, 1));
        vb.add(new Point3D(-1, 4, 1));
        vb.add(new Point3D(-1, 3, 3));
        vb.add(new Point3D(5, 2, 1));
        vb.add(new Point3D(5, 4, 1));
        vb.add(new Point3D(5, 3, 3));

        // Topology
        addIndices(0, 1, 1, 2, 2, 0, 3, 4, 4, 5, 5, 3, 0, 3, 1, 4, 2, 5);
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }
}
