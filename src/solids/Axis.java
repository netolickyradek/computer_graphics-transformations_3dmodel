package solids;

import transforms.Point3D;

public class Axis extends Solid {

    private int color;

    public Axis(String type) {
        vb.add(new Point3D(0, 0, 0));

        if (type.equals("x")) {
            vb.add(new Point3D(1, 0, 0));
            color = 0xff0000;
        }
        if (type.equals("y")) {
            vb.add(new Point3D(0, 1, 0));
            color = 0x00ff00;
        }
        if (type.equals("z")) {
            vb.add(new Point3D(0, 0, 1));
            color = 0x0000ff;
        }

        addIndices(0, 1);
    }

    @Override
    public int getColor() {
        return color;
    }
}
