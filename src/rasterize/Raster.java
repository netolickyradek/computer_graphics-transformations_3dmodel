package rasterize;

public interface Raster {
    /**
     * Sets individual pixel on screen with color
     * @param x - axis value
     * @param y - axis value
     * @param color - color of pixel
     */
    void setPixel(int x, int y, int color);

    /**
     * Returns pixel color in integer
     * @param x - axis value
     * @param y - axis value
     * @return int color value
     */
    int getPixel(int x, int y);
}
