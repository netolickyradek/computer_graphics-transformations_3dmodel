package rasterize;


import java.awt.*;

public class LineRasterizerGraphics extends LineRasterizer{

    public LineRasterizerGraphics(Raster raster) {
        super(raster);
    }


    @Override
    protected void drawLine(int x1, int y1, int x2, int y2, int lineColor) {
        Graphics g = ((RasterBufferImage)raster).getImg().getGraphics();
        g.setColor(new Color(lineColor));
        g.drawLine(x1, y1, x2, y2);
    }
}
