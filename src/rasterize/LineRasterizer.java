package rasterize;

import model.Line;

/**
 * Abstract class for drawing lines.
 */

public abstract class LineRasterizer {

    protected final Raster raster;

    public void rasterize(Line line){
        drawLine(line.getPoint1().getX(),line.getPoint1().getY(),line.getPoint2().getX(),line.getPoint2().getY(),line.getLineColor());
    }


    public LineRasterizer(Raster raster) {
        this.raster = raster;
    }

    protected abstract void drawLine(int x1, int y1, int x2, int y2, int lineColor);


}
